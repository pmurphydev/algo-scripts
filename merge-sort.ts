const unsortedArray = [1,7,5,23,82,15,99,3];

const mergeSort: any = (arr: Array<number>) => {
    const len = arr.length;

    if (len === 1) {
        return arr;
    }

    const l = mergeSort(arr.slice(0, Math.floor(len/2)));
    const r = mergeSort(arr.slice(Math.floor(len/2)));

    return merge(l, r);
};

const merge: any = (leftArray: Array<number>, rightArray: Array<number>) => {
    const holder = [];
    let leftIndex = 0, rightIndex = 0;

    while(leftIndex < leftArray.length && rightIndex < rightArray.length) {
        if(leftArray[leftIndex] < rightArray[rightIndex]) {
            holder.push(leftArray[leftIndex]);
            leftIndex++;
        } else {
            holder.push(rightArray[rightIndex]);
            rightIndex++;;
        };
    };

    return holder
        .concat(leftArray.slice(leftIndex))
        .concat(rightArray.slice(rightIndex));
};

const retVal = mergeSort(unsortedArray);

console.log('retVal: ', retVal);